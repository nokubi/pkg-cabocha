#!/usr/bin/make -f
# -*- Makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

ifndef PERL
PERL = /usr/bin/perl
endif
PERL_OPTIMIZE = -O2 -Wall
ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
PERL_OPTIMIZE += -g
endif
PYTHON=python3.5
PYTHON2=python2.7

%:
	dh $@ --with autoreconf --with quilt --with python3 --with python2

override_dh_auto_configure:
	dh_auto_configure -- --with-charset=UTF8 --libexecdir=/usr/lib
	cd perl && $(PERL) Makefile.PL INSTALLDIRS=vendor
	mkdir python2
	( cd python && tar cf - . )|( cd python2 && tar xf - )

override_dh_auto_build:
	dh_auto_build -- SUBDIRS='src man'
	$(MAKE) -C perl \
		INC=-I../src \
		EXTRALIBS="-L../src/.libs -lcabocha" \
		LDLOADLIBS="-L../src/.libs -lcabocha" \
		OPTIMIZE="$(PERL_OPTIMIZE)" LD_RUN_PATH="" \
		DESTDIR=`pwd`/debian/libcabocha-perl
	cd python && $(PYTHON) setup.py build
	cd python2 && $(PYTHON2) setup.py build

override_dh_auto_install: basedir=$(shell pwd)/debian
override_dh_auto_install:
	dh_auto_install -- SUBDIRS='src man'
	cp -p model/*.txt debian/cabocha-dic/usr/share/cabocha/model
	$(MAKE) -C perl install DESTDIR=`pwd`/debian/libcabocha-perl
	cd python && $(PYTHON) setup.py install \
		--prefix=$(basedir)/python3-cabocha/usr \
		--install-layout=deb
	cd python2 && $(PYTHON2) setup.py install \
		--prefix=$(basedir)/python-cabocha/usr \
		--install-layout=deb

override_dh_auto_clean:
	dh_auto_clean
	-$(MAKE) -C perl realclean
	-rm -f perl/Makefile.old
	-rm -rf python/build
	-rm -rf python2

# Do no tests.
override_dh_auto_test:
